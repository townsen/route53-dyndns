# Dynamic DNS using AWS Route 53

If you host your zone on Route 53 then this project will enable host machines behind a NAT
gateway to update the A records for specific hosts automatically. This is useful as your
service provider may change your IP address at any time. It also implements an EventBridge
subscription to an email SNS Topic to generate a brief informative email when the address
changes.

The `dns_update` script uses the AWS command line and standard DNS utilities to update the
entry for the chosen hostname.

*NOTE* the script loops every 5 minutes by default (an earlier version used a systemd
timer). This interval may be changed by specifying a second parameter on the command line.

## Setup

### AWS Credentials

Create a user or role with AWS permissions to update this zone in Route53 and place the
credentials in the profile name `[updater]` in a file named `credentials` in this
directory.

For security reasons the script executes as the user `nobody` so ensure that the file is
owned by that user.

### On Docker

We pass the AWS credentials to the script in a read-only Docker volume. Create this on the
host using the profile name `[updater]` for the credentials and populate the docker volume with:

    docker volume create awscreds
    docker run -it --mount source=awscreds,target=/creds -v ~/.aws:/aws busybox cp aws/credentials /creds

Then run the updater supplying the FQDN as a parameter:

    docker run -d \
        --name dns_updater --restart=always -v awscreds:/root/.aws \
        --log-driver local \
        townsen/dns_update:latest <fqdn>

Note that the docker daemon may have been configured to default to the local driver in
`/etc/docker/daemon.json`

### On Linux

Clone this repository to `/usr/local/route53-dyndns`.

You can either specify the desired DNS name on the command line (by editing the unit file
`dns_update.service`) or you can make an entry that aliases the hostname as specified in
the file `/etc/hostname` to the desired FQDN in `/etc/localhost` like so:

    127.0.0.1	localhost
    127.0.1.1	dnsname.sendium.net hostname

Install the updater service by running the following as the root user:

    apt-get install awscli dnsutils
    cp dns_update.service /etc/systemd/system/
    systemctl enable dns_update.service
    systemctl start dns_update.service

Look at the output using (you don't need to be root to do this):

    journalctl -f -u dns_update

### On MacOS

Clone this repository to `/usr/local/route53-dyndns`.

We use a Launch Daemon, rather than a LaunchAgent, so that the script will run even if the
executing user is not logged in. Customize the `net.sendium.dns_update.plist` file and
install with:

    sudo cp net.sendium.dns_update.plist /Library/LaunchDaemons/
    sudo launchctl load -w /Library/LaunchDaemons/net.sendium.dns_update.plist
    sudo launchctl start net.sendium.dns_update

The `dns_update` script will run continuously to check and update the DNS entry.  A log
of the updates is kept in `/var/log/dns_update.log`. You should create these files and
make them writeable by `nobody` as follows:

    sudo touch /var/log/dns_update.log
    sudo chown nobody /var/log/dns_update.log

Check the status of the daemon with the command:

    sudo launchctl list net.sendium.dns_update

You can optionally install the `lunchy` gem which provides a slightly more friendly user interface:

    sudo gem install lunchy
    sudo lunchy status dns_update

Note that it will not show a PID if the script is not running, but it's status should be
0.  You can also install with the `lunchy` gem:

    sudo lunchy install net.sendium.dns_update.plist
    sudo lunchy start -w dns_update

TODO Using `lunchy` the plist will be copied to the user LaunchAgent directory. I haven't
tested that lunchy will install a Launch Daemon correctly.

## AWS Setup

You will need to create a 'trail' in CloudTrail that logs to an S3 Bucket in order to generate EventBridge events. You can use the supplied CloudFormation template:

   rake cloudtrail

Then you can create the dns-emailer CloudFormation stack which takes the destination email
address as an input parameter

   rake create["nick.townsend@mac.com"]
