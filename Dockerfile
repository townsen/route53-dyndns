FROM xueshanf/awscli:latest
COPY dns_update /root
WORKDIR /root
ENV   AWS_PROFILE=updater
ENV   AWS_SHARED_CREDENTIALS_FILE=/root/.aws/credentials
ENTRYPOINT [ "./dns_update" ]
