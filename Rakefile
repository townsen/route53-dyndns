require 'aws-sdk-eventbridge'
require 'aws-sdk-cloudformation'
require 'yaml'

# Remove backtrace that isn't in the current tree
#
Rake.application.options.trace = false
Rake.application.options.suppress_backtrace_pattern = /^(?!#{Regexp.escape(FileUtils.pwd)}\/[^.])/

CFN_TEMPLATE = 'route53_dyndns.yaml'
template = YAML.load(File.open(CFN_TEMPLATE, &:read))
puts("You are running with AWS_PROFILE=#{ENV['AWS_PROFILE']}")

desc "Lint the Cloudformation Templates"
task :lint => :test do
    sh "cfn-lint -t #{CFN_TEMPLATE}"
end

desc "Test the pattern"
task :test do
  client = Aws::EventBridge::Client.new(region: 'us-east-1')
  pattern = template["Resources"]["EventBridgeSubscriptions"]["Properties"]["EventPattern"]
  event = File.open('eventbridge_upsert.json', &:read)
  resp = client.test_event_pattern({ event_pattern: pattern.to_json.to_s, event: event })
  fail("Pattern was NOT matched by Upsert Event!") unless resp[:result]
  puts "Pattern matches Event"
end

desc "Create a CloudTrail trail"
task :cloudtrail, [:retention]  do |t, args|
    cfn = Aws::CloudFormation::Client.new(region: 'us-east-1')
    params = []
    if retention = args[:email]
      params.push({ parameter_key: "Retention", parameter_value: retention })
    end
    rsp = cfn.create_stack(
        stack_name: "cloudtrail",
        template_body: File.read('cloudtrail.yaml'),
        capabilities: ['CAPABILITY_NAMED_IAM', 'CAPABILITY_AUTO_EXPAND'],
        parameters: params
    );
    puts("create_stack: #{rsp[:stack_id].split(':')[5]}")
end

# For update call into the create task to keep things DRY
desc "Create the DNS update mailer"
task :create, [:email] => :lint do |t, args|
    action = args[:update] ? 'update': 'create'
    cfn = Aws::CloudFormation::Client.new(region: 'us-east-1')
    email = args[:email] || template["Parameters"]["Email"]["Default"]
    rsp = cfn.send("#{action}_stack",
        stack_name: "dns-emailer",
        template_body: File.read(CFN_TEMPLATE),
        capabilities: ['CAPABILITY_NAMED_IAM', 'CAPABILITY_AUTO_EXPAND'],
        parameters: [
          { parameter_key: "Email", parameter_value: email }
        ]
    );
    puts("#{action.capitalize}: #{rsp[:stack_id].split(':')[5]}")
end

# For update call into the create task to keep things DRY
# Note that if you 'invoke' then the args aren't passed (?)
#
desc "Update the DNS update mailer"
task :update, [:email] => :lint do |t, args|
    Rake::Task['create'].execute(Rake::TaskArguments.new([:update],[true]))
end
